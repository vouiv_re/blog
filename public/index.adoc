= Ce qui suit n'est pas un récit de voyage
:toc: left
:toclevels: 5

> Il y a des fautes. Tant pis.

[id=back_to_top]
.Emy et Pierre-Élie partent en vacances
image::./img/img_lego.jpg[legos]

include::../src/chapter_1.adoc[leveloffset=+1]
include::../src/chapter_2.adoc[leveloffset=+1]
include::../src/chapter_3.adoc[leveloffset=+1]
include::../src/chapter_4.adoc[leveloffset=+1]
include::../src/chapter_5.adoc[leveloffset=+1]
include::../src/chapter_6.adoc[leveloffset=+1]
include::../src/chapter_7.adoc[leveloffset=+1]
include::../src/chapter_8.adoc[leveloffset=+1]
include::../src/chapter_9.adoc[leveloffset=+1]
include::../src/chapter_10.adoc[leveloffset=+1]
include::../src/chapter_11.adoc[leveloffset=+1]
include::../src/chapter_12.adoc[leveloffset=+1]
include::../src/chapter_13.adoc[leveloffset=+1]
include::../src/chapter_14.adoc[leveloffset=+1]
include::../src/chapter_15.adoc[leveloffset=+1]
include::../src/chapter_16.adoc[leveloffset=+1]
include::../src/chapter_17.adoc[leveloffset=+1]
include::../src/chapter_18.adoc[leveloffset=+1]
include::../src/chapter_19.adoc[leveloffset=+1]
include::../src/chapter_20.adoc[leveloffset=+1]
include::../src/chapter_21.adoc[leveloffset=+1]
include::../src/chapter_22.adoc[leveloffset=+1]
include::../src/chapter_23.adoc[leveloffset=+1]
include::../src/chapter_24.adoc[leveloffset=+1]
include::../src/chapter_25.adoc[leveloffset=+1]
include::../src/chapter_26.adoc[leveloffset=+1]
include::../src/chapter_27.adoc[leveloffset=+1]

[.text-right]
link:#back_to_top[Retour en haut]