= Chapitre 19 : La vérité

> 19ème jour : vendredi 23 septembre 2022

== TL;DR

* Balade autour de l'île
* L’environnement est beau
* Journée tranquille

== Pour les paillettes, même s'il n'y en a pas beaucoup aujourd’hui

> 11h07

Ce matin dès le lever je me suis attelé au récit de la veille.

Pendant ce temps Emy est au téléphone. Et c'est incroyable de constater à quel point, les gens remettent en cause (à mon plus grand soulagement) le récit que j'écris. Malgré le fait de n'avoir à disposition qu'un seul écrit, les gens à partir de leurs connaissances de nous même, arrivent à retracer le récit contradictoire. C'est assez inédit. J'ai réécrit ce passage deux fois car pouvant prêter à trop de confusions (et application du droit de censure 😁).

> 22h14

Nous avons mangé ce midi de délicieuses coquillettes sur leurs lits d’oignons saupoudré de champignons rehaussé à la bratwurst et baignant dans un duvet de tomates. La poêle à du servir à un juge sentencieux.

Nous somme partis nous promener sur les abords de l'île, et avons quasiment fini par en faire le tour. Il y avait de spacieuses cabanes à barbecue, de curieux bouleaux, des voie lactée de lichens or, verte, gris et marrons sur des pierres grises parcourue par des strates de quartz rose et blanc. Des plages de sables, des forêts, des roches. La balade était reposante quoique un poil longue. Emy voulait prendre l'hypoténuse du chemin hypothétiquement le plus court, et moi les côtés adjacent et opposé du chemin que l'on avait déjà pris. J'ai l'impression qu'on a fait un zigzag entre les deux. C'est donc ça un compromis ? Étrange.

On a à priori vu des méduses. Emy était contente. Visiblement, pour elle, les oiseaux ne font pas partis de la faune sauvage.

On a fait trois courses pour le lendemain. Regardé un épisode. Et on a beaucoup parlé. C'est quand même ouf d'avoir toujours autant de trucs à se dire, même après 12 ans. Je ne sais pas si c'est qu'on s'écoute jamais ou qu'on oublie très vite. Ca m’étonnerais que ce soit autre chose. Dans tout les cas c'est rassurant.

Pas grand chose à dire de plus. Une bonne journée.