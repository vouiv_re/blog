= Chapitre 11 : Eau terne à Otternes ?

> 11ème jour : jeudi 15 septembre 2022

> 22h47

Alors pour ceux qui râlent parce qu'il n'y a pas de photos :

 . Mon téléphone fait des photos pourries
 . Je ne sais pas cadrer une photo (et ça me va très bien comme ça ;p)
 . Emy fait de très belles photos, et elle en fait plein. Malheureusement c'est sur sa carte SD

Il faut donc attendre l'accès à un ordinateur, ce qui n'est pas près d'arriver. Mais ne t'inquiète pas elles arriveront bien un jour ou l'autre.

Levé tranquillou ce matin, on est parti à la pêche aux renseignements. La récolte a été bonne, trop bonne. Il y a d'abord eu la sardine du bateau qui parcourt le Fjord. Le thon de la randonnée. La crevette du sauna flottant et l'espadon du bus touristique. Après tant d'attente à tenter de tèj tel ou tel type de passe-temps, allitération, nous mangeâmes du thon. Et même pas celui de la conserve qu'on se trimbale encore faute d'ouvre-boîte. En fait c'est simple, le temps du choix, les bus et les bateaux sont parti, il ne nous restait plus que nos pieds. Ça n'est pas tout à fait vrai, mais nous voulions marcher.

J'ai quand même eu le loisir d'observer à l'accueil un individu louche qui ne se privait pas de passer devant les gens pour obtenir un renseignement de toute urgence. Un vrai goujat.

```
Petit ajout à posteriori d'un dialogue à *Myrdal* entre celle-dont-on-ne-doit-pas-prononcer-le-nom et une réceptionniste, ou cette dernière lui demande si on veut voyager avec un bateau "zéro émission" et ou *CDONDPPLN* lui demande droit dans ses bottes, l'air de rien si c'est le moins cher.
```

Nous avons longé le Fjord pendant 1h30 avant de grimper sur la colline, sur les épaules de laquelle se juchait un très vieux petit village plein de charme, dont on aurait des traces de présence bien avant JC. Une solide fondation de pierre, par dessus jusqu'à neuf billots de bois en forme de champignon, sur lesquels se reposait l'entièreté de la maison intégralement en bois. D'une de ces maison, à surgit d'un coup, d'un seul, une tripoté de marmaille viking geignant et hurlant à m'en faire glacer le sang. Je crois bien qu'il y avait un petit Loki avec eux. Le seul brun du groupe se remarque vite.

Nous avons mangé sur un vieux banc surplombant le fjord. Un pommier nous abritant bien mal du peu de crachin présent ce jour la. C'était magique.

Nous avons pris le même chemin sur le retour. Il est très agréable bien qu'un peu court.

À ce moment nous avons pu assister à l'arrivée d'un énorme bateau de croisière dans le fjord. Il a jeté l'ancre à *Fläm*. Il était tellement grand qu'il camouflait l'intégralité du village. Il le surpassait à la fois en hauteur, en largeur et en nombre d'occupants. Pas franchement la plus belle de découverte de jour. Je pense que les récits de Kraken d'antan doivent provenir de la vision cauchemardesque des navires de croisière, ça te bouffe un paysage en deux coups de cuillère à pot !

Une fois retourné au village nous avons visité le musée du chemin de fer touristique que nous avions fait la veille. Et qui retraçait l'histoire de ces petits villages et la galère que ça a été de creuser à la main tous ces tunnels depuis plus d'un siècle.

Après un peu de shopping, un peu de courses et l'achat des billets de train pour le lendemain. Entre temps je me suis endormi assis sur un banc pendant qu'Emy vérifiait, mhhh que vérifiait-elle déjà ?

Sur le trajet du retour Emy a essayé, sans succès, de faire la distinction entre une vache, un mouton et une chèvre. Ca me rappelle le bon vieux temps, aujourd’hui révolu, ou elle me nommait les arbres et les fleurs sous l'unique appellation de "plantes". Qui sait un jour saurât elle aussi reconnaître les animaux.

Nous sommes ensuite rentré. Nous avons mangé. J'ai inventé le spliff "tranche de fromage pas extraordinaire entourant au choix : roquette ou cacahuète". Pas ouf mais pas pire. Je ne sais pas si deux trucs médiocre s'améliorent l'un l'autre, sont une occasion de chute mutuelle, ou si ça ne change rien. Et oui je ne suis pas seulement inventeur mais aussi scientifique dans l'âme.

J'ai encore perdu à *Star Realms*, Ce jeu m'a honteusement trahis, je ne le reconnais plus. Il n'est plus le même. Il répondra désormais au nom de "*Juda-rs Realms*" et vivra pour le restant de ses jours dans l’opprobre.

J'ai pris une douche. Avec mon savon cette fois. Vachement plus agréable pour l'amour-propre quand même.

Demain direction Bergen.