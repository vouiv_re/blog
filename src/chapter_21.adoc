= Chapitre 21 : Les brouettes à moteur : pratique ou Patrick ?

> 21ème jour : dimanche 25 septembre 2022

== TL;DR

* Sur l'île il y a beaucoup de ridicule pétrolette-caddies
* Nous nous sommes promené au nord de l'île
* Nous avons revu Bernard

== Paillettes

> 18h17

Sur cette île, en arrivant au port, il y a un panneau interdisant les voitures, les motos, les scooters. Le panneau est grand, il est bien entouré de rouge, pas même un daltonien ne pourrait s'y tromper (gnagna il y a plusieurs type de daltonisme, oui je sais !).

Et pourtant il ont des sortes de brouette ou des caddies sur le devant d'une espèce de pétrolette, qui n'est "of course my horse" pas électrique vu le parfum dégagé. Bon déjà c'est disgracieux. Le charme d'un macareux dans une nappe de pétrole.

Et il y en a pléthore. Au moins dix pour un vélo. Je sais bien que la population est un poil (blanc) vieillissante, mais je ne comprend pas. Il doit y avoir un vide juridique, genre une interdiction des moyens de transport motorisé à deux et à quatre roues, mais pas à trois. Ou encore l'interdiction de tout véhicule motorisé qui ne fasse pas du transport de marchandises. Je suis certain d'avoir entendu ricaner un vieil avocat véreux dans le pétardage de l'engin. Un truc du genre "BRuM bruM bram BRAm bum bum HI hin hin-hin h-i-n". Je suis assez fier de ma translittération.

Emy m'a emmené dans la partie nord de l'île la ou je n'étais encore jamais allé. Elle ne put me le cacher bien longtemps, elle voulait revoir Bernard. La promenade était douce. La nature drape les abords du chemin de mille couleurs, parures, chatoiements, qui varient à chaque pas.

Nous avons entre-aperçu fugitivement Bernard. Et j'avais une puce qui bondissait à côté de moi.

Je suis actuellement assis sur un banc, non loin d'Emy, toujours à l'affût. Le soleil déjà bas vient joue à "1,2,3 nuages". Le salaud est en train de gagner.

Je pense que c'est tout ce que j'aurais à raconter pour aujourd’hui.