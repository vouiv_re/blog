= Chapitre 20 : Quiétude

> 20ème jour : samedi 24 septembre 2022

== TL;DR

* Emy à fait de magnifique photos sur l'île
* J'ai gambergé avec succès sur projet

== Paillettes

> Le lendemain, 17h16

Encore du retard aujourd’hui, c'est maintenant officiellement une habitude.

Le rythme ralenti, le temps aussi. C'est la partie de notre séjour la plus calme. C'était un des points d'achoppement entre Emy et moi, que j'ai vaillamment du défendre, lors de la conception de nos vacances. Je suis d'ailleurs assez étonné qu'elle l'ai respecté, même s'il faut vous être vigilant, et être en mesure de dégainer les gros yeux plus rapidement que l'adversaire ses lunettes. Je crois que je suis plutôt mauvais à ce jeu.

Les journées sont aussi douce que la peau d'un hamac pendant un couché de soleil. Je m'excuse auprès de tout les travailleurs et travailleuses qui me lisent, peut-être au futur moi même. Le paradis existe sur terre, il ne s'agit que d'une affaire de contexte.

Pas de réveil le matin et pas ou peu d’horaires dans la journée. C'est une des joies simple des vacances. Je béni l'inventeur de l'horloge, car le plaisir de se passer de sa création est à consommer sur l'heure.

Il y a eu l'observation de la course du soleil, puis sa rationalisation sous le forme d'un gnomon dressé. Les jours de pluie il y avait la clepsydre ou le sablier. Des humains ont travaillé d'arrache pied, pour établir des calendrier indiquant quand la soleil irait se coucher au solstice d'hivers ou quand planter les patates. Ils ont découvert le ressort puis la pile. Tout ce temps passer à essayer de le maîtriser, pour que sans cesse il s'échappe comme une ombre que l'on voudrait saisir. Tout ce temps pour qu'un péquenaud sur une île décide de s'en passer Tout ce temps pour se rendre compte que, parfois, pour vaincre il suffit d'arrêter de combattre l'autre.

Laisse moi le temps de m'ébrouer un instant.

Là, ça va mieux. Hier Emy est parti  de son côté faire des photos. Et ma foi, ça lui a plutôt réussit je crois. Elle m'en a montré trois qui sont magnifiques. Je ne te dirais pas ce qu'elle a réussit à prendre, je lui laisse ce plaisir, dûment mérité, mais je peux te dire les prénoms de modèles. Il y a Bernard, Pablo et Enrique. Ca t'avance ?

De mon côté je voulais écrire, et bien évidement je n'en ai rien fait. Une idée fixe refusait de m'en laisser la liberté. Ayant gambergé toute la journée et après une discussion de presque trois heures avec Emy, cette dernière ma offert les clefs donnant accès à un dés-embrumateur de cerveau dernier cri.

Je me suis endormis avec un rantanplan d'attaque en tête contre mon idée fixe.