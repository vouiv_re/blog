= Chapitre 12 : L'histoire du berger sur les berges de Bergen

> 12ème jour : vendredi 16 septembre 2022

> 20h59

J'écris à une heure décente ce soir, étonnant.

Je suis un peu vanné, donc ce sera peut-être un peu plus mollasson ce soir. Nous sommes partis à 10h04 de notre maisonnette. Abandonnant lâchement une confiture de framboise inentamée. Nous gardons par contre notre boîte de thon, comme si c'était notre précieux. Peut-être a t'elle des pouvoirs magiques ? Il me faut enquêter. Ou alors c'est un thon de Schrödinger. A la fois mangeable et immangeable. Mhh en fait c'est littéralement ça, vu qu'on a toujours pas d'ouvre-boîte.

J'ai failli prendre mon fidèle leatherman, mais j'avais trop part de m'en séparer au premier portique. Je crois qu'on en a juste passé un, et c'était pour un musée donc nous n'avions pas toutes nos affaires. J'ai préféré prendre l'opinel, génération 10, parce que parfaitement identique aux 9 premiers, ainsi qu'au 11ème potentiel. Les opinels prennent du grade par le remplacement, pas par l'âge. Ce doit être dure une vie d'opinel.

Emy a enfin craqué pour un bonnet. Depuis hier elle m'en parle. Voici un bref résumé de la situation :

. E (enjouée): Il me faut un bonnet, je n'ai pas pris de bonnet
. PE (à moitié endormi): T'as pas un buff ?
. E (auto-convaincue): Si mais c'est pas pareil, et j'aimerais un bonnet.
. Emy va dans le boutique
. E (interrogative): Il est joli celui-la, c'est pas trop moche au dessus ?
. Emy désigne le bonnet violet sur sa tête
. PE (affirmatif): Non, c'est très bien !
. E: Mhhh j'aime pas !
. PE: Ok
. Le lendemain en attendant le train, Emy regardant sa tête dans le miroir :
. E: Tu préfères celui-ci ou celui-la ?
. Emy montre à Pierre-Élie deux bonnets, un gris et le même violet de la veille.
. Pierre-Élie prenant un ton très sûr alors qu'il vient de lancer une pièce de monnaie mentale
. PE : Assurément celui-là !
. Emy prend le bonnet gris

Je reconnais que la pièce telle quelle n'est pas très intéressante. Elle est au moins à peu près véridique. Et comme je n'ai pas envie de grimper la montagne demain sur les genoux, j'ai décider de ne pas trop travestir la scène. Mais voici tout de même, quelques pistes pour en faire une scène pseudo-comique d'un scène de boulevard :

* Ajouter de grands gestes inutiles à la femme pour ponctuer ses tirades, et lui donner par ces grands moulinets un petit côté hystérique mais sympathique.
* Demander l'avis à l'homme, tout en constatant le peu de goût en matière vestimentaire des hommes
* Rajouter des grognements ostentatoires de la part de l'homme qui essaie de lire son journal
* Faire en sorte que la femme change d'avis au dernier moment, se ralliant de fait à l'avis de l'homme, mais en invoquant une autre raison, genre l'assortiment à des bottines qu'elle ne met plus.
* La femme sort de scène extatique
* Spot sur l'homme qui hausse les épaules en regardant le public
* Noir

Bon je reconnais que ça n'est finalement pas mieux. Au diable ma carrière de dramaturge !

Nous avons repris le train touristique direction de *Myrdal* et magie, cette fois je n'ai pas dormi. J'avoue c'était joli.

Peu après, nous reprenons un train pour Bergen où nous longeons. L'auberge de jeunesse a l'air pas mal. C'est un peu l'usine. Probablement moins qu'à Copenhague, mais quand même, ça turbine.

On rencontre un coloc qui est sur le départ, et plus tard un autre qui reste dans la chambre. Léonard, enfin pas tout à fait, mais presque. Il est des Pays-Bas.

On pose les affaires, et Emy monte une combine pour faire semblant d'avoir un cadenas au casier, j'aurais bien dit "brillant !" mais je dirais plutôt "mignon !".

Le réceptionniste (je crois que c'est la première fois que c'est un homme), nous conseille une ballade qu'on commence aussitôt (16h). Elle nous fait monter, monter, monter sur les hauteurs de Bergen. Ils ont ici, semble t-il, une vraie passion/tradition pour les gigantesques escaliers de roche. Celui-ci ne montait pas aux cieux, mais pas loin, genre juste deux étages plus bas. Sans compter le demi étage pour l'ascenseur. J'ai ahané tout le long. Nous avons vu la même joggeuse que la dernière fois nous dépasser au moins quatre fois dans les deux sens. J'ai bien voulu lui courir après pour lui casser les pattes, mais je pense qu'elle a pris du niveau, elle était beaucoup trop rapide pour mon sprint d'ours asthénique. J'en fait désormais une affaire personnelle. I will come back.

Et dire que cette rando tardive s'est faite sur un malentendu. Emy m'avait promis un restau. Et j'ai cru naïvement que c'était celui au sommet de la montagne. Elle m'a bien eu. J'étais si motivé... La désillusion a été rude.

La vue était magnifique, un panorama à 180 degré juste indescriptible. Tu verras les photos. Le vent à te congeler instantanément dans ta propre sueur. Heureusement nous étions bien équipés. Mes chaussures sont top, je ne sais pas si j'en ai déjà parlé, mais elles sont top !

On est redescendu, on a mangé à l'AdJ, discuté un brin avec Léonard. Et voila !