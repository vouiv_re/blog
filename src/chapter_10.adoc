= Chapitre 10 : Chute d'eau et chut dodo !

> 10ème jour : Mercredi 14 septembre 2022

> 21h29

Réveil ce matin à l'hôtel dans un lit moelleux, petit déjeuner salé comme on commence à en prendre l'habitude, je refixe une devanture de baignoire qui s'était cassé la margoulette la veille, et on sort enfin.

Nous ne sommes pas très haut en altitude, à peu près 850 mètres (866 en vrai, Emy vient de me renseigner), et pourtant nous avons l'impression d'être en haute montagne. Nous somme cerné par ces tarasques de toute parts. De celles-ci s'écoule une multitude de cascades qui viennent abreuver un grand lac au milieu de ces vielles dames. C'est magnifique.

Ce qui ajoute à notre stupéfaction c'est la végétation très dense, tous les sommets sont verdurés. Ce qui n'est pas le cas dans nos montagnes qui sont tondues ou rocheuses. Il y a beaucoup de baies ressemblant à des myrtilles qui rase le sol. Une sorte de bruyère rouge et verte, de la mousse en pagaille, et un lichen blanc qui fait des corolles de choux-fleurs s'ajoutent à ce tableau. Un vrai décors de crèche !

Nous décidons de nous rendre à la cascade qui découle de ce lac pour se jeter 100 mètres en contrebas. Le fracas est énorme, dantesque. Et c'est aussi la première fois que j’assiste à une pluie à gravité inversé ! Le vent est si fort et l'écume si violente que des particules d'eau sont projeté vers le haut. Il ne pleuvait pas, c'était la cascade qui depuis sa chute nous arrosait copieusement.

Bien que le paysage soit assez uniforme dans son ensemble. il comporte plein de particularité locale : lac, montagne, marais, forêt. Passer en quelques instants d'un biome à l'autre est assez rafraîchissant. Après la cascade nous avons décidé de longer une partie du lac jusqu'à midi puis de faire demi-tour pour retourner manger à l'hôtel avant de reprendre un petit train touristique. Il y a pas mal de maisons rouges sur le chemin. Toutes relié par des chemins de rondins et de planches qui permettent d'éviter les plus gros des marais. Un ballet d'hélicoptères nous accompagne.

Un burger montagnard plus loin, et nous entrons dans le petit train qui est pas mal fréquenté. Emy arrive à vendre 3 tapis et demi au vendeur de tickets, et nous pouvons enfin nous installer confortablement. Je vous fait un bref résumé, mais à gauche il y avait la vue sur une vallée et à priori un caillou rigoLOL dont on peut faire le tour, et à droite il y avait des cascades. La vérité c'est que je me suis endormi assez rapidement, bercé par le concert des exclamations des touristes qui changeaient de bord du train à chaque virages. Ils m'ont épuisé. Je sais c'est honteux, mais je regarderais les photos probablement en même temps que toi.

Arrivé à *Fläm*, nous prenons trois choses :

* Des renseignements que l'on utilisera pas.
* De la nourriture qui ne sera pas mangé de suite et que l'on regrettera probablement (un truc à base de curry, de crevettes, d'une bouillie indistincte, de boulettes de viande et probablement d'un peu de dignité française, j'ai pas compris le délire)
* La route pour notre auberge de jeunesse

Un brin de soleil éclaire notre journée nuageuse. Jusqu'a présent nous avons beaucoup de chance pour la météo, au pire juste un petit crachin passager.

Pour les trajets aussi, on s'en est bien sorti !

On pose nos affaire et décidons de partir profiter de notre fin d'après midi en grimpant jusqu'a une cascade (encore une, décidément il n'y que de la flotte ici). Nous avons grimpé un très bel escalier de dalles de pierre, le chemin fait des circonvolutions jusqu'a un point de vue magnifique qui nous sert la cascade, la vue sur le lac en contrebas et tout la vallée qui la contient. Franchement je n'ai jamais vu un panorama aussi parfaitement composé. Même un maître peintre y aurait ajouté quelques menus défauts pour rendre le tableau crédible.

Sur place nous croisons un clan d'instagrameuses et leur meilleure copine qui leurs tient les manteaux. Et nous y restons un bon moment.

Au moment de redescendre nous croisons une femme à qui nous disons bonjour (ou plutôt "Hey !" comme ils disent par ici). Plus loin elle nous dépasse en courant dans la descente, puis nous la re-croisons quand elle remonte et ainsi de suite. A un moment j'ai juré à Emy de lui briser ses tibias si nous la re-croisions. "Gnagna je suis sportive, gnagna j'ai un bon cardio, bouh les loosers". Nan mais franchement quelle indécence. Heureusement pour mon casier judiciaire nous ne la vîmes plus. Et accessoirement pour ses tibias.

Dans le petit chalet qui nous abrite nous croisons un autre couple et un jeune homme dans la cuisine. Nous parlons peu. Emy m'éclate sur *Star Realms* une seconde fois. Je commence à regretter *Richesse du monde*. Nous mangeons le résultat de nos courses du début d'aprèm que je ne vais pas re-restituer ici. Et je dis restituer, mais j'ai tout gardé, hein ! Puis un brin de vaisselle, ce qui ne m'avait pas manqué.

Et enfin la douche. Ah assis toi un instant il faut que je te raconte. Petit tips de pauvre, de raclo ou de flemmard. Je te laisse le soin de me ranger dans l'une de ces catégories, mais par pitié, sans me le dire. J'ai l'habitude de voyager avec deux gants de toilettes dans l'un il y a tout mon nécessaire à hygiène : brosse à dents, dentifrice, coupe-ongle, déo, ... Dans l'autre il y a une paire de savons dur. Comme ça quand ils sont un peu humide, je ne salope pas toute mes affaires. Et ayant été aux toilettes plus tôt dans la soirée j'avais déjà repéré que dans la douche il y avait un distributeur de gel douche. Je me dit "Parfait pas besoin de tremper mon savon !". Donc j'arrive sous cette pluie brûlante et savoureuse, je tend la main pour me servir en savon et là malheur le distributeur ne donne rien. Je passe un bon 3 minutes à essayer de démonter la machine, sans succès. Plusieurs options se présente à moi à ce moment la. Me sécher pour remonter dans ma chambre récupérer le savon tant désiré, flemme. Appeler Emy à ma rescousse, certainement pas, elle exigerait son dû en massages au centuple du prix d'un honnête savon. Ne pas me savonner, bon ben la c'était quand même important, donc non.

Voici ma solution, à toi de me dire ce que tu aurais fait à ma place : Je me suis égoutté, je suis sorti de l'habitacle de la douche, j'ai fait deux mètres jusqu'au robinet et je me suis copieusement servis en savon à main en provenance d'un distributeur automatique et m'en suis recouvert le corps. Une fois ceci fait, je suis retourné sous la douche. Pas mal hein ? A la fois économe et flemmard, tout moi quoi.

Sur ce je vais glisser dans mes draps, propre comme un sou neuf ;)